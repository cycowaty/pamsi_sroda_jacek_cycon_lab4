#include <iostream>
#include <cstdlib>
#include <ctime>

struct Node
{
    int Priority;
    int Value;
    Node *Prev;
    Node *Next;
    Node(int _Value, int _Priority=0)
        : Value(_Value), Priority(_Priority)
    {

    }
    Node()
        : Prev(nullptr), Next(nullptr), Value(0), Priority(0)
    {

    }
};

class List
{
protected:
    Node* First;
    Node* Last;
    int Size;
public:
    List()
        : First(nullptr), Last(nullptr), Size(0)
    {
        ;
    }
    void Add(int Index, Node* NewNode)
    {
        if(!First)
        {
            First = NewNode;
            //First = new Node;
            //First->Value=Value;
            Last = First;
            ++Size;
        }
        else
        {
            if(Index==0)
            {
                //Node *NewNode = new Node;
                //NewNode->Value=Value;
                NewNode->Next=First;
                First->Prev=NewNode;
                First = NewNode;
                ++Size;
            }
            else if(Index==Size)
            {
                //Node *NewNode = new Node;
                //NewNode->Value=Value;
                NewNode->Prev=Last;
                Last->Next=NewNode;
                Last = NewNode;
                ++Size;
            }
            else if(Index>0 && Index<Size)
            {
                Node *Temp = First;
                while(Index)
                {
                    Temp = Temp->Next;
                    --Index;
                }
                //Node *NewNode = new Node;
                //NewNode->Value=Value;
                NewNode->Next=Temp;
                NewNode->Prev=Temp->Prev;

                Temp->Prev->Next=NewNode;
                Temp->Prev=NewNode;
                ++Size;
            }
            else std::cerr<<"Error: [Element with index "<<Index<<" can't be added]\n";
        }
    }
    void Remove(int Index)
    {
        if(Size==1 && Index==0)
        {
            delete First;
            First = nullptr;
            Last = nullptr;
            --Size;
        }
        else if(Size)
        {
            if(Index==0)
            {
                First=First->Next;
                delete First->Prev;
                First->Prev = nullptr;
                --Size;
            }
            else if(Index==Size-1)
            {
                Last=Last->Prev;
                delete Last->Next;
                Last->Next = nullptr;
                --Size;
            }
            else if(Index>0 && Index<Size-1)
            {
                Node *Temp = First;
                while(Index)
                {
                    Temp = Temp->Next;
                    --Index;
                }
                Temp->Prev->Next=Temp->Next;
                Temp->Next->Prev=Temp->Prev;
                delete Temp;
                --Size;
            }
            else std::cout<<"Error: [Element to remove doesn't exist]\n";
        }
    }
    int GetSize()
    {
        return Size;
    }
    void Clear()
    {
        if(!First) return;
        Node* Temp = First;
        Node* ToRemove = nullptr;
        while(Temp)
        {
            ToRemove=Temp;
            Temp=Temp->Next;
            delete ToRemove;
        }
        First = nullptr;
        Last = nullptr;
        Size = 0;
    }
};

class PriorityQueue : public List
{
public:
    void Push(int Priority, int Value)
    {
        int Index=Size;
        Node *Temp = Last;
        while(Index && Temp->Priority<Priority)
        {
            Temp=Temp->Prev;
            --Index;
        }
        Add(Index, new Node(Value, Priority));
    }
    void Pop()
    {
        if(First) Remove(0);
    }
    void PushWithSort(int Value)
    {
        Push(Value, Value);
    }
    void WriteAll()
    {
        std::cout<<"Queue: ";
        if(!First)
        {
            std::cout<<"[Is empty]\n";
            return;
        }
        Node *Temp = First;
        while(Temp)
        {
            //std::cout<<Temp->Value<<'['<<Temp->Priority<<"] ";
            std::cout<<Temp->Value<<' ';
            Temp=Temp->Next;
        }
        std::cout<<'\n';
    }
    void SetTab(int *TargetTab)
    {
        if(!Size) return;
        //int *Tab = new int[Size];
        int Index=0;
        Node* Temp = First;
        while(Temp)
        {
            TargetTab[Index]=Temp->Value;
            Temp=Temp->Next;
            ++Index;
        }
    }
    void SortTab(int *Tab, int SizeTab)
    {
        Clear();
        for(int i=0; i<SizeTab; ++i) PushWithSort(Tab[i]);
        SetTab(Tab);
        Clear();
    }
};

int main()
{
    srand(std::time(0));

    PriorityQueue MyQueue;
    /*
    MyQueue.Push(0,3);
    MyQueue.Push(0,19);
    MyQueue.Push(2,5);
    MyQueue.Push(2,4);
    MyQueue.Push(2,7);
    MyQueue.Push(1,1);
    MyQueue.Push(1,8);
    MyQueue.Push(1,9);
    MyQueue.Push(1,9);
    MyQueue.WriteAll();
    */

    //for(int i=0; i<10; ++i) MyQueue.PushWithSort(rand()%100);
    //MyQueue.WriteAll();

    int Size = 15;
    int *Tab = new int[Size];
    for(int i=0; i<Size; ++i) Tab[i]=rand()%50;

    //std::cout<<'\n';
    for(int i=0; i<Size; ++i) std::cout<<Tab[i]<<' ';
    std::cout<<'\n';
    std::cout<<"Sorting...\n";
    MyQueue.SortTab(Tab, Size);
    for(int i=0; i<Size; ++i) std::cout<<Tab[i]<<' ';

    std::cin.get();
    std::cin.get();
}
