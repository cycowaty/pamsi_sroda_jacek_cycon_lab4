#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;



		class Node
{
	public:
int Place, War;
Node *Rgt;
Node *Lft;
Node *Parent;
Node()
            //konstruktor
	{
		Parent = nullptr;
		Lft = nullptr;
		Rgt = nullptr;
		War = 0;
		Place = 0;
	}
};



		class BiNTree
{

Node *Root;
int NextPlace, MaxPoziom;
	public:
BiNTree() //konstruktor

	{
		Root=nullptr;
		NextPlace = 0;
		MaxPoziom = 0;
	}

~BiNTree() //destruktor
	{
		Clear();
	}

void NewRoot(int War)
	{
			if (Root) return;
		Root = new Node;
		Root->War = War;
		Root->Place = NextPlace;
			++NextPlace;
	}

void Add(int Place, int War)
	{
			if (!Root) return;
		Node *Temp = GetNode(Place);
			if (Temp)
		{
		if (!Temp->Lft)
		{
			Temp->Lft = new Node;
			Temp->Lft->War = War;
			Temp->Lft->Place = NextPlace;
			Temp->Lft->Parent = Temp;
				++NextPlace;
		}
		else if (!Temp->Rgt)
		{
			Temp->Rgt = new Node;
			Temp->Rgt->War = War;
			Temp->Rgt->Place = NextPlace;
			Temp->Rgt->Parent = Temp;
				++NextPlace;
		}
		}
		else cout << "Wezel nie istnieje!"<<endl;
	}

void Remove(int Place)
	{
			if (!Root) return;
			if (Place == 0)
	{
		if (!Root->Lft && !Root->Rgt)
		{
			delete Root;
			Root = nullptr;
			NextPlace = 0;
		}
			return;
	}

Node *Temp = GetNode(Place);
			if (Temp)
			{

		{
			if (Temp->Parent->Lft == Temp)
			{
				Clear(Temp);
				Temp->Parent->Lft = nullptr;
					delete Temp;
			}
			else if (Temp->Parent->Rgt == Temp)
			{
				Clear(Temp);
				Temp->Parent->Rgt = nullptr;
					delete Temp;
			}
		}
	}
		else cout << "Wezel nie istnieje!"<<endl;
		cout << endl;
	}

Node *GetNode(int Place)

	{
		return GetNode(Place, Root);
	}

Node *GetNode(int Place, Node* rNode)
	{
		Node *Temp = nullptr;
		if (rNode->Place == Place) Temp = rNode;
		if (rNode->Lft && !Temp) Temp = GetNode(Place, rNode->Lft);
		if (rNode->Rgt && !Temp) Temp = GetNode(Place, rNode->Rgt);
			return Temp;
	}

void Write()
	{
		if (!Root) return;
		Write(Root);
	}

void Write(Node *rNode, int rPoziom = 0)
	{
		if (rNode->Rgt) Write(rNode->Rgt, rPoziom + 1);
		for (int i = 0; i<rPoziom; ++i) cout << "     ";
		cout << rNode->War << "{" << rNode->Place << "}\n";
		if (rNode->Lft) Write(rNode->Lft, rPoziom + 1);
	}

int GetRootWar()
		{
	if (Root)
	{
		return Root->War;
	}
		return 0;
		}

void Clear()
	{
		if (!Root) return;
		Clear(Root);
		Root = nullptr;
		NextPlace = 0;
	}

void Clear(Node *rNode)
	{
		if (rNode->Rgt) Clear(rNode->Rgt);
		if (rNode->Lft) Clear(rNode->Lft);
		delete rNode;
	}

int GetMaxPoziom()
	{
		MaxPoziom = 0;
		FMaxPoziom(Root);
			return MaxPoziom;
	}

void FMaxPoziom(Node* rNode, int Poziom = 0) // Znajdz max poziom
	{
		if (rNode->Lft) FMaxPoziom(rNode->Lft, Poziom + 1);
		if (Poziom>MaxPoziom) MaxPoziom = Poziom;
		if (rNode->Rgt) FMaxPoziom(rNode->Rgt, Poziom + 1);
	}
};

int main()
{
	BiNTree BTree;


	cout << "Drzewo przed operacja usuniecia: "<<endl;

//stworz korzen
	BTree.NewRoot(28);
//dodaj elementy (place,war)
	BTree.Add(0, 3);
	BTree.Add(0, 14);
	BTree.Add(2, 1);
	BTree.Add(3, 6);
	BTree.Add(4, 14);
	BTree.Add(4, 1);

	cout << "Wysokosc drzewa= " << BTree.GetMaxPoziom()<<endl;
	BTree.Write();
	cout << endl << endl<<endl;
	//cout << "Drzewo po operacji usniecia: "<<endl;
	//BTree.Remove(6);
	//BTree.Remove(6);
	//BTree.Remove(5);
	/*BTree.Remove(4);
	BTree.Remove(4);	BTree.Remove(4);
	BTree.Remove(2);
	BTree.Remove(1);*/
	//cout << "Wysokosc drzewa= " << BTree.GetMaxPoziom();
	//cout << endl;
	//BTree.Write();

	cout << endl;

}
