#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

struct Node
{
	int Priority;
	int Value;
	Node *Prev;
	Node *Next;
	Node(int _Value, int _Priority = 0)
		: Value(_Value), Priority(_Priority) //lista inicjalizacyjna
	{
	}

	Node()        //konstruktor

	{
	Prev=nullptr;
	Next=nullptr;
    Value=0;
    Priority=0;
	}

};

class List
{
protected:
	Node* First;
	Node* Last;
	int Size;
public:
	List()

	{
        First=nullptr;
        Last=nullptr;
        Size=0;
	}

void Add(int Index, Node* NewNode)
	{

		if (!First)
		{
			First = NewNode;
			Last = First;
			++Size;
		}
		else
		{
			if (Index == 0)
			{
				NewNode->Next = First;
				First->Prev = NewNode;
				First = NewNode;
				++Size;
			}
			else if (Index == Size)
			{
				NewNode->Prev = Last;
				Last->Next = NewNode;
				Last = NewNode;
				++Size;
			}
			else if (Index>0 && Index<Size)
			{
				Node *Temp = First;
				while (Index)
				{
					Temp = Temp->Next;
					--Index;
				}
				NewNode->Next = Temp;
				NewNode->Prev = Temp->Prev;
				Temp->Prev->Next = NewNode;
				Temp->Prev = NewNode;
				++Size;
			}
			else cerr << "Element o indeksie " << Index << " nie moze byc dodany!" << endl;
		}
	}



void Remove(int Index)
	{

		if (Size == 1 && Index == 0)
		{
			delete First;
			First = nullptr;
			Last = nullptr;
			--Size;
		}
		else if (Size)
		{
			if (Index == 0)
			{
				First = First->Next;
				delete First->Prev;
				First->Prev = nullptr;
				--Size;
			}
			else if (Index == Size - 1)
			{
				Last = Last->Prev;
				delete Last->Next;
				Last->Next = nullptr;
				--Size;
			}
			else if (Index>0 && Index<Size - 1)
			{
				Node *Temp = First;
				while (Index)
				{
					Temp = Temp->Next;
					--Index;
				}
				Temp->Prev->Next = Temp->Next;
				Temp->Next->Prev = Temp->Prev;
				delete Temp;
				--Size;
			}
			else cout << "Element, ktory chcesz usunac nie istnieje!" << endl;
		}
	}
int GetSize()
	{
		return Size;
	}
void Czysc()
	{
		if (!First) return;
		Node* Temp = First;
		Node* ToRemove = nullptr;
		while (Temp)
		{
			ToRemove = Temp;
			Temp = Temp->Next;
			delete ToRemove;
		}
		First = nullptr;
		Last = nullptr;
		Size = 0;
	}
};

class QueuePrio : public List
{
public:
void Push(int Priority, int Value)
	{

		int Index = Size;
		Node *Temp = Last;
		while (Index && Temp->Priority<Priority)
		{
			Temp = Temp->Prev;
			--Index;
		}
		Add(Index, new Node(Value, Priority));
	}
void Pop()
	{
		if (First) Remove(0);
	}

void PushAndSort(int Value)
	{
		Push(Value, Value);
	}

void Write()
	{
		cout << "Kolejka: ";
		if (!First)
		{
			cout << "Kolejka jest pusta! " << endl;
			return;
		}
		Node *Temp = First;
		while (Temp)
		{
			cout << Temp->Value << '[' << Temp->Priority << "] "; //pokaz wartosc i klucz
																  //   cout<<Temp->Value<<' ';                 //pokaz wartosc
			Temp = Temp->Next;
		}
		cout << endl;
	}

void SetTab(int *TargetTab)
	{
		if (!Size) return;
		int Index = 0;
		Node* Temp = First;
		while (Temp)
		{
			TargetTab[Index] = Temp->Value;
			Temp = Temp->Next;
			++Index;
		}
	}

void SortujTab(int *Tab, int SizeTab)
	{
		Czysc();
		for (int i = 0; i<SizeTab; ++i) PushAndSort(Tab[i]);
		SetTab(Tab);
		Czysc();
	}
};

int main()
{





	QueuePrio MyQueue;

	//MyQueue.Push(0,3);   //(klucz,wart)  //wprowadz recznie
	//MyQueue.Push(0,19);
	//MyQueue.Push(2,5);
	//MyQueue.Push(2,4);
	//MyQueue.Push(2,7);
	//MyQueue.Push(1,1);
	//MyQueue.Push(1,8);
	//MyQueue.Push(1,9);
	//MyQueue.Push(1,9);
	//MyQueue.Write();
	//MyQueue.Pop();
	//MyQueue.Write();

	//for(int i=0; i<3; ++i) MyQueue.PushWithSort(rand()%12); //Dodaj do biezacych i sortuj
	//MyQueue.Write();
srand(time(0));
int Size = 10;
int *Tab = new int[Size];
for(int i=0; i<Size; ++i) Tab[i]=rand()%25; //Klucz ==wart. //losowe liczby
	//sortowanie
for(int i=0; i<Size; ++i) cout<<Tab[i]<<' ';
cout<<endl;
cout<<"Proces sortowania: "<<endl;
MyQueue.SortujTab(Tab, Size);
for(int i=0; i<Size; ++i) cout<<Tab[i]<<' ';

}
