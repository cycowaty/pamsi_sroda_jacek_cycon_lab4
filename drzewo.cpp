#include <iostream>

using namespace std;

template <typename T>
struct Node
{
	T Value;
	Node<T> *Next;    //wskaznik na nastepny wezel
	Node<T> *Prev;    //wskaznik na poprzedni wezel
	Node()            //konstruktor
	{
	 Value=0;
	 Next=nullptr;
     Prev=nullptr;
	}
};

template <typename T>
class List
{
	Node<T> *First;   //wskaznik na pierwszy wezel
	Node<T> *Last;    //wskazik na ostatni wezel
	int Size;
public:
	List()            //konstruktor
	{
	Size=0;
	First=nullptr;
    Last=nullptr;
	}

void AddToEnd(T Value)
	{
		Add(Size, Value);
	}

void Add(int Index, T Value) //dodaj element
	{
		if (!Size && Index == 0)
		{
			First = new Node<T>;
			First->Value = Value;
			Last = First;
			++Size;
		}
		else if (Size && Index == 0)
		{
			Node<T> *NewNode = new Node<T>;
			NewNode->Value = Value;
			NewNode->Next = First;
			First->Prev = NewNode;
			First = NewNode;
			++Size;
		}
		else if (Index == Size)
		{
			Node<T> *NewNode = new Node<T>;
			NewNode->Value = Value;
			NewNode->Prev = Last;
			Last->Next = NewNode;
			Last = NewNode;
			++Size;
		}
		else if (Index>0 && Index<Size)
		{
			Node<T> *Temp = First;
			while (Index)
			{
				Temp = Temp->Next;
				--Index;
			}
			Node<T> *NewNode = new Node<T>;
			NewNode->Value = Value;

			Temp->Prev->Next = NewNode;
			NewNode->Prev = Temp->Prev;
			NewNode->Next = Temp;
			Temp->Prev = NewNode;

			++Size;
		}
	}
void Remove(int Index) //usun element
	{
		if (Size == 1 && Index == 0)
		{
			delete First;
			First = nullptr;
			Last = nullptr;
			Size = 0;
		}
		else if (Size && Index == 0)
		{
			First = First->Next;
			delete First->Prev;
			First->Prev = nullptr;
			--Size;
		}
		else if (Size && Index == Size - 1)
		{
			Last = Last->Prev;
			delete Last->Next;
			Last->Next = nullptr;
			--Size;
		}
		else if (Index>0 && Index<Size)
		{
			Node<T> *Temp = First;
			while (Index)
			{
				Temp = Temp->Next;
				--Index;
			}
			Temp->Prev->Next = Temp->Next;
			Temp->Next->Prev = Temp->Prev;
			delete Temp;
			--Size;
		}
	}

void WriteAll()    //wypisz dla listy
	{
		cout << "Lista: ";
		if (Size)
		{
			Node<T> *Temp = First;
			while (Temp)
			{
				cout << Temp->Value << ' ';
				Temp = Temp->Next;
			}
		}
		else cout << "Lista pusta!";
	}

int GetSize()
	{
		return Size;
	}

Node<T> *GetFirst()
	{
		return First;
	}

Node<T>* operator[](int Index)
	{
		Node<T> *Temp = First;
		while (Index)
		{
			Temp = Temp->Next;
			--Index;
		}
		return Temp;
	}
};



//Drzewo ogolne




struct TreeNode
{
	int Index;
	int Value;
	TreeNode* Parent; //wskaznik wezla drzewa na rodzica
	List<TreeNode*> Children;
	TreeNode()        //konstruktor
	{
	Index=0;
    Parent=nullptr;
	}

int GetSize()
	{
		return Children.GetSize();
	}

TreeNode* operator[](int Index)
	{
		return Children[Index]->Value;
	}

TreeNode* GetChild(int Index)
	{
		return Children[Index]->Value;
	}

void AddNewChild(int Value, int NewIndex)
	{
		TreeNode* NewNode = new TreeNode;
		NewNode->Index = NewIndex;
		NewNode->Value = Value;
		NewNode->Parent = this;
		Children.AddToEnd(NewNode);
	}

void RemoveChild(TreeNode *Child)
	{
		for (int i = 0; i<Children.GetSize(); ++i)
		{
			if (GetChild(i) == Child)
			{
				Children.Remove(i);
				break;
			}
		}
	}
};

class Tree
{
	TreeNode *Root; //wskaznik drzewa na korzen
	int Size;
public:
	Tree()
	{
	Root=nullptr;
    Size=0;
	}

void NewRoot(int Value)
	{
        if (!Root)
		{
			Root = new TreeNode;
			Root->Value = Value;
			Root->Index = Size;
			++Size;
		}
	}

	TreeNode* GetNode(int Index)
	{
		return GetNode(Index, Root);
	}

	TreeNode* GetNode(int Index, TreeNode *_TreeNode)
	{
		TreeNode *Ret = _TreeNode;
		for (int i = 0; i<_TreeNode->GetSize(); ++i)
		{
			Ret = GetNode(Index, _TreeNode->GetChild(i));
			if (Ret && Ret->Index == Index) return Ret;
		}
		return _TreeNode;
	}
void Add(TreeNode *_Node, int Value)
	{
		_Node->AddNewChild(Value, Size);
		++Size;
	}

void Write() //wypisz dla drzewa
	{
		cout << "Drzewo:" << endl;
		if (!Root)
		{
			cout << "Drzewo jest puste!" << endl;
			return;
		}
		WriteAll(Root);
	}
void WriteAll(TreeNode *_TreeNode)
	{
		cout << _TreeNode->Index << '[' << _TreeNode->Value << "] ";
		if (_TreeNode->GetSize())
		{
			for (int i = 0; i<_TreeNode->GetSize(); ++i)
			{
				TreeNode *Temp = _TreeNode->GetChild(i);
				cout << Temp->Index << '{' << Temp->Value << "} ";
			}
			cout << endl;

			for (int i = 0; i<_TreeNode->GetSize(); ++i)
			{
				WriteAll(_TreeNode->GetChild(i));
			}
		}
		else cout << "--Brak synow" << endl;
	}
void Remove(TreeNode *_TreeNode)
	{
		if (_TreeNode == Root)
		{
			Czysc();
			return;
		}
		Czysc(_TreeNode);
		cout << endl;
		if (_TreeNode->Parent)
		{
			_TreeNode->Parent->RemoveChild(_TreeNode);
		}
	}
int GetLvlTree()       //wysokosc drzewa
	{
		if (!Root) return 0;
		return GetLvlTree(1, Root);
	}
int GetLvlTree(int Lvl, TreeNode *_TreeNode)
	{
		int Ret = Lvl;
		if (_TreeNode->GetSize()) ++Lvl;
		for (int i = 0; i<_TreeNode->GetSize(); ++i)
		{
			Lvl = GetLvlTree(Lvl, _TreeNode->GetChild(i));
		}
		return Lvl;
	}
void Czysc()
	{
		if (!Root) return;
		Czysc(Root);
		Size = 0;
		Root = nullptr;
	}
void Czysc(TreeNode *_TreeNode)
	{
		for (int i = 0; i<_TreeNode->GetSize(); ++i)
		{
			Czysc(_TreeNode->GetChild(i));
		}
		delete _TreeNode;
	}
};

int main()
{

Tree MyTree;
MyTree.NewRoot(2);               //poziom 1

MyTree.Add(MyTree.GetNode(0), 14);   //poziom 2
MyTree.Add(MyTree.GetNode(0), 6);
MyTree.Add(MyTree.GetNode(0), 12);  //(nr wezla, wartosc)

MyTree.Add(MyTree.GetNode(2), 5);   //poziom 3
MyTree.Add(MyTree.GetNode(2), 1);

MyTree.Add(MyTree.GetNode(6), 7);  //poziom 4
MyTree.Add(MyTree.GetNode(6), 12);
MyTree.Add(MyTree.GetNode(6), 19);

	// cout<<"Wysokosc drzewa: "<<MyTree.GetLvlTree()<<endl;
	// MyTree.WriteAll();

	//   MyTree.Remove(MyTree.GetNode(6));
	//cout<<"Wysokosc drzewa: "<<MyTree.GetLvlTree()<<endl;
	// MyTree.WriteAll();

	//  MyTree.Remove(MyTree.GetNode(2));
	// cout<<"Wysokosc drzewa: "<<MyTree.GetLvlTree()<<endl;
	//MyTree.WriteAll();

	//MyTree.Remove(MyTree.GetNode(1));
	// MyTree.Remove(MyTree.GetNode(3));
cout << "Wysokosc drzewa: " << MyTree.GetLvlTree() << endl;
MyTree.Write();

MyTree.Remove(MyTree.GetNode(0));
cout << "Wysokosc drzewa: " << MyTree.GetLvlTree() << endl;
MyTree.Write();

MyTree.Czysc();
}











































/*Sprawdzenie dzialania listy dwukierunkowej
List<int> MyList;

for(int i=1; i<=20; ++i)
{
MyList.Add(MyList.GetSize(), i);
}
MyList.WriteAll();
cout<<endl;

for(int i=1; i<=10; ++i)
{
MyList.Remove(MyList.GetSize()-1);
}
MyList.WriteAll();
*/
